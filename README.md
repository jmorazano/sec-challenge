# Securitize Challenge
This challenge can be tested by running the docker-compose file on the root

First we should spin up the Postgres DB and run migrations
```
$ docker-compose up -d postgres
$ cd api
$ npm run migrations:run
$ docker-compose down
```

Now we can start all services inside the docker-compose
```
$ docker-compose up
```

U$D and Euro rates are fetched from

https://min-api.cryptocompare.com

![Scheme](images/demo.png)