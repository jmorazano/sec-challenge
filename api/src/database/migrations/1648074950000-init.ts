import {MigrationInterface, QueryRunner} from "typeorm";

export class init1648074950000 implements MigrationInterface {
    name = 'init1648074950000'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "wallet" ("id" SERIAL NOT NULL, "address" character varying NOT NULL, "favorite" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_bec464dd8d54c39c54fd32e2334" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "wallet"`);
    }

}
