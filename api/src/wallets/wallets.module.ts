import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WalletsService } from './services/wallets.service';
import { WalletsController } from './controllers/wallets.controller';
import { Wallet } from './entities/wallets.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Wallet])],
  providers: [WalletsService],
  controllers: [WalletsController],
})
export class WalletsModule {}
