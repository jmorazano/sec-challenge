import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import axios, { AxiosResponse } from 'axios';
import { Wallet } from '../entities/wallets.entity';

const ETHERSCAN_URL = process.env.ETHERSCAN_URL;
const ETHERSCAN_API_TOKEN = process.env.ETHERSCAN_API_TOKEN;

export interface WalletPlusInfo extends Wallet {
  isOld: boolean;
  accounBalance: number;
}

interface EtherScanApiResponse<T> {
  status: string;
  message: string;
  result: T;
}

interface EtherScanAccountBalance {
  account: string;
  balance: number;
}

interface EtherScanTx {
  blockNumber: number;
  timeStamp: number;
  hash: string;
  nonce: string;
  blockHash: string;
  transactionIndex: string;
  from: string;
  to: string;
  value: number;
  gas: number;
  gasPrice: number;
  isError: number;
  txreceipt_status: string;
  input: string;
  contractAddress: string;
  cumulativeGasUsed: number;
  gasUsed: number;
  confirmations: number;
}

@Injectable()
export class WalletsService {
  constructor(
    @InjectRepository(Wallet) private walletRepo: Repository<Wallet>,
  ) {}

  async findAll() {
    const wallets = await this.walletRepo.find();

    if (wallets.length === 0) {
      return [];
    }

    const accountAddressList = wallets.map((w) => w.address);
    const accountsBalance = await this.getAccountsBalance(accountAddressList);

    const latestTxPromises = accountAddressList.map((a) =>
      this.getAccountLatestTx(a),
    );
    const responses = await Promise.all(latestTxPromises);

    const walletsResponse = wallets.map((w, i) => {
      const balance = accountsBalance.find(
        (a) => a.account === w.address,
      ).balance;
      const txDateTimestamp = responses[i].timeStamp;
      const txDate = new Date(txDateTimestamp * 1000);

      const ageDifMs = Date.now() - txDate.getMilliseconds();
      const ageDate = new Date(ageDifMs); // miliseconds from epoch
      const years = Math.abs(ageDate.getUTCFullYear() - 1970);
      return {
        ...w,
        isOld: years >= 1,
        accounBalance: balance / 1000000000000000000, // Balance is in wei
      } as WalletPlusInfo;
    });

    return walletsResponse;
  }

  findOne(id: number) {
    return this.walletRepo.findOne({ where: { id } });
  }

  async create(body: Wallet) {
    const newWallet = this.walletRepo.create(body);
    this.walletRepo.save(newWallet);

    const latestTx = await this.getAccountLatestTx(body.address);
    const accountsBalance = await this.getAccountsBalance([body.address]);

    const txDateTimestamp = latestTx.timeStamp;
    const txDate = new Date(txDateTimestamp * 1000);

    const ageDifMs = Date.now() - txDate.getMilliseconds();
    const ageDate = new Date(ageDifMs); // miliseconds from epoch
    const years = Math.abs(ageDate.getUTCFullYear() - 1970);
    return {
      ...newWallet,
      isOld: years >= 1,
      accounBalance: accountsBalance[0].balance / 1000000000000000000, // Balance is in wei
    } as WalletPlusInfo;
    return ;
  }

  async update(id: number, body: any) {
    const wallet = await this.walletRepo.findOne(id);
    this.walletRepo.merge(wallet, body);
    return this.walletRepo.save(wallet);
  }

  async delete(id: number) {
    await this.walletRepo.delete(id);
    return true;
  }

  async getAccountsBalance(
    addresses: string[],
  ): Promise<EtherScanAccountBalance[]> {
    const response: AxiosResponse<
      EtherScanApiResponse<EtherScanAccountBalance[]>
    > = await axios.get(
      `${ETHERSCAN_URL}?module=account&action=balancemulti&address=${addresses.join(
        ',',
      )}&tag=latest&apikey=${ETHERSCAN_API_TOKEN}`,
    );
    return response.data.result;
  }

  async getAccountLatestTx(address: string): Promise<EtherScanTx | null> {
    const response: AxiosResponse<EtherScanApiResponse<EtherScanTx[]>> =
      await axios.get(
        `${ETHERSCAN_URL}?module=account&action=txlist&address=${address}&startblock=0&endblock=99999999&page=1&offset=1&sort=asc&apikey=${ETHERSCAN_API_TOKEN}`,
      );

    return response.data.result.length > 0 ? response.data.result[0] : null;
  }
}
