import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { WalletPlusInfo, WalletsService } from '../services/wallets.service';

@Controller('wallets')
export class WalletsController {
  constructor(private walletsService: WalletsService) {}

  @Get()
  getAll(): Promise<WalletPlusInfo[]> {
    return this.walletsService.findAll();
  }

  @Get()
  getOne(@Param('id') id: number) {
    return this.walletsService.findOne(id);
  }

  @Post()
  create(@Body() body: any) {
    return this.walletsService.create(body);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() body: any) {
    return this.walletsService.update(id, body);
  }

  @Delete(':id')
  delete(@Param('id') id: number) {
    return this.walletsService.delete(id);
  }
}
