export interface ExContextInterface {
  USD: number;
  EUR: number;
}

export interface Wallet {
  id: number;
  address: string;
  favorite: boolean;
  isOld: boolean;
  accounBalance: number;
}

export interface CreateWalletPayload {
  address: string;
}

export interface WalletsService {
  getAllWallets(): Promise<Wallet[]>;
  updateWallet(wallet: Wallet): Promise<Wallet>;
  createWallet(payload: CreateWalletPayload): Promise<Wallet>;
}

export interface ExchangeService {
  getRates(): Promise<ExContextInterface>;
}