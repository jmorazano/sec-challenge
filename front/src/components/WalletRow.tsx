import { ChangeEvent, useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faWarning,
  faPenToSquare,
  faCheck,
  faXmark,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";
import { faHeart as faHeartRegular } from "@fortawesome/free-regular-svg-icons";
import { ExContextInterface, Wallet } from "../types";
import { useExContext } from "./ExContext";
import { walletService } from "../services/walletsService";

interface WalletRowProps {
  wallet: Wallet;
}

export const WalletRow = (props: WalletRowProps) => {
  const exchangeRates = useExContext();
  const [wallet, setWallet] = useState<Wallet>(props.wallet);
  const [isEditMode, setIsEditMode] = useState(false);
  const [selectedCurrency, setSelectedCurrency] =
    useState<keyof ExContextInterface>("USD");
  const [currentCurrencyExchange, setCurrentCurrencyExchange] = useState(0);
  const [currentBalance, setCurrentBalance] = useState(0);
  const amountUSLocale = Intl.NumberFormat("en-US", {
    style: "currency",
    currency: selectedCurrency,
  });

  const handleCurrencyChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const key = e.target.value as keyof ExContextInterface;
    setSelectedCurrency(key);
  };

  const handleCancel = () => {
    if (exchangeRates) {
      setCurrentCurrencyExchange(exchangeRates[selectedCurrency]);
      setIsEditMode(false);
    }
  };

  const toggleFavorite = async () => {
    try {
      const newWallet = {
        ...wallet,
        favorite: !wallet.favorite,
      };
      await walletService.updateWallet(newWallet);
      setWallet(newWallet);
    } catch (error) {
      console.log("Failed to update wallet:", error);
    }
  };

  useEffect(() => {
    if (exchangeRates) {
      setCurrentCurrencyExchange(exchangeRates[selectedCurrency]);
      setCurrentBalance(exchangeRates[selectedCurrency] * wallet.accounBalance);
    }
  }, [exchangeRates, selectedCurrency, wallet.accounBalance]);

  useEffect(() => {
    setCurrentBalance(currentCurrencyExchange * wallet.accounBalance);
  }, [currentCurrencyExchange, wallet.accounBalance]);

  return (
    <div className="wallet row">
      {wallet.isOld && (
        <div className="age-warning">
          <FontAwesomeIcon icon={faWarning} />
          <span>Wallet is old!</span>
        </div>
      )}
      <div className="exchange-pannels">
        <div className="editor">
          {isEditMode ? (
            <>
              <div className="actions">
                <FontAwesomeIcon onClick={handleCancel} icon={faXmark} />
                <FontAwesomeIcon
                  onClick={() => setIsEditMode(false)}
                  icon={faCheck}
                />
              </div>
              <input
                onChange={(e) =>
                  setCurrentCurrencyExchange(parseFloat(e.target.value))
                }
                className="exchange-rate"
                value={currentCurrencyExchange}
                type="number"
                name="rate"
                id="ex-rate-input"
              />
            </>
          ) : (
            <>
              <div className="actions">
                <FontAwesomeIcon
                  onClick={() => setIsEditMode(!isEditMode)}
                  icon={faPenToSquare}
                />
              </div>
              <p className="exchange-rate">{amountUSLocale.format(currentCurrencyExchange)}</p>
            </>
          )}
        </div>
        <div className="currency">
          <select
            onChange={handleCurrencyChange}
            name="currency"
            id="currency-selector"
            value={selectedCurrency}
          >
            {exchangeRates &&
              Object.keys(exchangeRates).map((currency) => (
                <option key={currency} value={currency}>
                  {currency}
                </option>
              ))}
          </select>
          <p className="wallet-balance">
            {amountUSLocale.format(currentBalance)}
          </p>
        </div>
      </div>
      <div className="favorite">
        <span>{wallet.address}</span>
        <FontAwesomeIcon
          onClick={toggleFavorite}
          icon={wallet.favorite ? faHeart : faHeartRegular}
        />
      </div>
    </div>
  );
};
