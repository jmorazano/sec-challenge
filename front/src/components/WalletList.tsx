import { WalletRow } from "./WalletRow";
import { Wallet } from "../types";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHeart,
  faSortDown,
  faSortUp,
} from "@fortawesome/free-solid-svg-icons";

interface WalletListProps {
  wallets: Wallet[];
}

export const WalletList = (props: WalletListProps) => {
  const [wallets, setWallets] = useState(props.wallets);
  const [sortDown, setSortDown] = useState(false);

  const handleSort = () => {
    const newOrderArray = [...wallets].sort((x, y) =>
      x.favorite === y.favorite ? 0 : x ? -1 : 1
    );
    setWallets(newOrderArray);
    setSortDown(!sortDown);
  };

  return (
    <div>
      {wallets.length === 0 ? (
        "Please add a wallet by address"
      ) : (
        <>
          <h1>Wallets:</h1>
          <span onClick={handleSort} className="sort-by-fav">
            Sort by <FontAwesomeIcon icon={faHeart} />
            <FontAwesomeIcon icon={sortDown ? faSortDown : faSortUp} />
          </span>
          <ul>
            {wallets.map((wallet) => (
              <li key={wallet.id}>
                <WalletRow wallet={wallet} />
              </li>
            ))}
          </ul>
        </>
      )}
    </div>
  );
};
