import React, {
  createContext,
  useState,
  useEffect,
  FC,
  useContext,
} from "react";
import { exchangeService } from "../services/exchangeService";
import { ExContextInterface } from "../types";

const defaultEx = { BTC: 0, EUR: 0, USD: 0 };

const ExContext = createContext<ExContextInterface | null>(defaultEx);

const ExContextProvider: FC = (props) => {
  const [exchange, setExchange] = useState<ExContextInterface | null>(null);

  // fetch exchange rates from backend API
  useEffect(() => {
    const fetchExchange = () => {
      exchangeService.getRates().then((response) => {
        setExchange(response);
      });
    };

    fetchExchange();
  }, []);

  return (
    <ExContext.Provider value={exchange}>{props.children}</ExContext.Provider>
  );
};

const useExContext = () => {
  const context = useContext(ExContext);

  if (context === undefined) {
    throw new Error("useUserContext was used outside of its Provider");
  }

  return context;
};

export { ExContext, ExContextProvider, useExContext };
