import { ChangeEvent, FormEvent, useState } from "react";

interface AddWalletProps {
  onAddWallet: (address: string) => void;
}

export const AddWallet = (props: AddWalletProps) => {
  const [address, setAddress] = useState("");

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setAddress(e.target.value);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    props.onAddWallet(address);
    setAddress("");
  };

  return (
    <div className="add-wallet">
      <form onSubmit={handleSubmit}>
        <input
          onChange={handleChange}
          value={address}
          type="text"
          placeholder="Wallet Address"
          name="address"
          id="wallet-address-input"
        />
        <button type="submit" className="btn">Add Wallet</button>
      </form>
    </div>
  );
};
