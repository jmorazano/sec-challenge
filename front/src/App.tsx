import React, { useEffect, useState } from "react";
import { AddWallet } from "./components/AddWallet";
import { WalletList } from "./components/WalletList";
import { ExContextProvider } from "./components/ExContext";
import "./App.css";
import { walletService } from "./services/walletsService";
import { Wallet } from "./types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

function App() {
  const [wallets, setWallets] = useState<Wallet[]>([]);
  const [isLoading, setIsLoading] = useState(true);

  const addWallet = async (address: string) => {
    try {
      setIsLoading(true);
      const newWallet: Wallet = await walletService.createWallet({ address });
      setWallets((ws) => [...ws, newWallet]);
    } catch (error) {
      console.log("newWallet ERROR: ", error);
    } finally {
      setIsLoading(false);
    }
  };

  const fetchWallets = async () => {
    try {
      const wallets = await walletService.getAllWallets();
      console.log(wallets);
      setWallets(wallets);
      setIsLoading(false);
    } catch (error) {
      console.log("Error fetching wallets", error);
      setWallets([]);
    }
  };

  useEffect(() => {
    fetchWallets();
  }, []);

  return (
    <div className="app-wrapper">
      <ExContextProvider>
        <AddWallet onAddWallet={addWallet} />
        {isLoading ? (
          <FontAwesomeIcon className="spinner" icon={faSpinner} />
        ) : (
          <WalletList wallets={wallets} />
        )}
      </ExContextProvider>
    </div>
  );
}

export default App;
