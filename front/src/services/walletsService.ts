import axios, { AxiosResponse } from "axios";
import { CreateWalletPayload, Wallet, WalletsService } from "../types";

const BASE_URL = "http://localhost:9000";

export const walletService: WalletsService = {
  getAllWallets: async () => {
    const response: AxiosResponse = await axios.get(`${BASE_URL}/wallets`);
    return response.data as Wallet[];
  },
  createWallet: async (payload: CreateWalletPayload) => {
    const response: AxiosResponse = await axios.post(
      `${BASE_URL}/wallets`,
      payload
    );
    return response.data as Wallet;
  },
  updateWallet: async (wallet: Wallet) => {
    const response: AxiosResponse = await axios.put(
      `${BASE_URL}/wallets/${wallet.id}`,
      wallet
    );
    return response.data as Wallet;
  },
};
