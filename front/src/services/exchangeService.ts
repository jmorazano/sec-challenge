import axios, {AxiosResponse} from "axios";
import { ExchangeService, ExContextInterface } from "../types";

export const exchangeService: ExchangeService = {
    getRates: async () => {
        const response: AxiosResponse<ExContextInterface> = await axios.get("https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD,EUR");
        return response.data;
    }
}